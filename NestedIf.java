package com.nine;

/**
 * @author Lathusan Thurairajah
 * @email lathusanthurairajah@gmail.com
 * @version 1.0
 * Jan 28, 2021
 **/

public class NestedIf {
	
	public static void main(String[] args) {
		
		double maths = 90;
		double science = 27;
		
		if (maths >= 75 && maths <=100) {
			if (science >= 75 && science <=100) {
				System.out.println("This student maths and science marks up to 75.");
			}else {
				System.out.println("This student science marks bellow 75.");
			}
		}else {
			System.out.println("This student maths marks bellow 75.");
		}
		
	}

}
